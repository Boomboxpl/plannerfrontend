using System.Globalization;
using Blazored.LocalStorage;
using Blazored.SessionStorage;
using Blazored.SessionStorage.Serialization;
using Blazorise;
using Blazorise.Bootstrap5;
using Blazorise.Icons.FontAwesome;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection.Extensions;
using PlannerFrontend;
using PlannerFrontend.Clients;

CultureInfo.DefaultThreadCurrentCulture = CultureInfo .InvariantCulture;
var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri("https://plannercms.fly.dev") });

builder.Services.AddBlazoredSessionStorage();
builder.Services.AddBlazoredLocalStorage();
builder.Services.Replace(ServiceDescriptor.Scoped<IJsonSerializer, NewtonSoftJsonSerializer>());
builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<ITokenService, TokenService>();
builder.Services.AddScoped<IClientConfigurationService, ClientConfigurationService>();

builder.Services
    .AddBlazorise( options =>
    {
        options.Immediate = true;
    } )
    .AddBootstrap5Providers()
    .AddFontAwesomeIcons();

var uri = new Uri(builder.Configuration["PlannerService"]!);
builder.Services.AddHttpClient<IAuthorizationClientService, AuthorizationClientService>(client => 
    client.BaseAddress = uri);
builder.Services.AddHttpClient<IProfileClientService, ProfileClientService>(client => 
    client.BaseAddress = uri);
builder.Services.AddHttpClient<IEventsClientService, EventsClientService>(client => 
    client.BaseAddress = uri);
builder.Services.AddHttpClient<IPropositionsClientService, PropositionsClientService>(client => 
    client.BaseAddress = uri);

var host = builder.Build();
await host.Services.GetService<IUserService>()?.CheckIfLoggedIn()!;
await host.RunAsync();

