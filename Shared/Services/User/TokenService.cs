﻿using Blazored.LocalStorage;
using PlannerFrontend.Shared;

namespace PlannerFrontend; 

public class TokenService : ITokenService {
    ILocalStorageService localStorage;

    public TokenService(ILocalStorageService localStorage) {
        this.localStorage = localStorage;
    }
    
    public async Task<string?> GetAccess() {
        return await localStorage.GetItemAsync<string?>(StorageKeys.AccessToken);
    }

    public async Task SetAccess(string? access) {
        await localStorage.SetItemAsync(StorageKeys.AccessToken, access);
    }

    public async Task<string?> GetRefresh() {
        return await localStorage.GetItemAsync<string?>(StorageKeys.RefreshToken);
    }

    public async Task SetRefresh(string? refresh) {
        await localStorage.SetItemAsync(StorageKeys.RefreshToken, refresh);
    }
    
    public async Task Logout() {
        await SetAccess(null);
        await SetRefresh(null);
    }
}