﻿using Blazored.LocalStorage;
using Blazored.SessionStorage;
using PlannerFrontend.Clients;
using PlannerFrontend.Shared;

namespace PlannerFrontend;

public class UserService : IUserService {
    public Action<bool> LoginStatusChanged { get; set; }
    public bool IsLoggedIn { 
        get => isLoggedIn;
        private set {
            isLoggedIn = value;
            LoginStatusChanged?.Invoke(value);
        } 
    }
    
    public ProfileSerializerAdmin? Data { get; private set; }

    public ITokenService TokenService { get; }

    bool isLoggedIn;
    
    readonly IAuthorizationClientService authorization;
    readonly IProfileClientService profile;

    public UserService(IAuthorizationClientService authorization, IProfileClientService profile, ITokenService token) {
        this.TokenService = token;
        this.authorization = authorization;
        this.profile = profile;
        Data = null;
    }

    public async Task CheckIfLoggedIn() {
        await LoadProfile();
    }

    public async Task<bool> LoadProfile() {
        try {
            Data = await profile.CurrentGetAsync();
            IsLoggedIn = true;
            return true;
        }
        catch (ApiException e) {
            if (e.StatusCode == 401) {
                IsLoggedIn = false;
            }
        }
        return false;
    }

    public async Task Logout() {
        await TokenService.Logout();
        Data = null;
        IsLoggedIn = false;
    }
    
}