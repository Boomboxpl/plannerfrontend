﻿namespace PlannerFrontend; 

public interface ITokenService {
    public async Task SetTokens(string? access, string? refresh) {
        await Task.WhenAll(SetAccess(access), SetRefresh(refresh));
    }
    
    public Task<string?> GetAccess();
    public Task SetAccess(string? access);
    public Task<string?> GetRefresh();
    public Task SetRefresh(string? refresh);
    
    public Task Logout();
}