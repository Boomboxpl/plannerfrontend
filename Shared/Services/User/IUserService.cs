﻿using PlannerFrontend.Clients;

namespace PlannerFrontend; 

public interface IUserService {
    public Action<bool> LoginStatusChanged {get; set;}
    public bool IsLoggedIn { get; }
    public ProfileSerializerAdmin? Data { get; }
    public int? Id => Data?.User?.Id;

    public ITokenService TokenService { get; }

    public Task CheckIfLoggedIn();
    public Task<bool> LoadProfile();
    public Task Logout();

}