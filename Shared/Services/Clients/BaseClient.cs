﻿using System.Text;
using Blazored.LocalStorage;

namespace PlannerFrontend.Clients; 

public class BaseClient {
    IClientConfigurationService configuration;
    
    public BaseClient(IClientConfigurationService configuration) {
        this.configuration = configuration;
    }

    public async Task PrepareRequestAsync(HttpClient client, HttpRequestMessage request, StringBuilder urlBuilder, CancellationToken cancellationToken) {
    }
    
    public async Task PrepareRequestAsync(HttpClient client, HttpRequestMessage request, string url, CancellationToken cancellationToken) {
        string? token = await configuration.TokenService.GetAccess();
        if (!string.IsNullOrEmpty(token)) {
            request.Headers.Add("Authorization", $"Bearer {token}");
        }
    }
    
    public async Task ProcessResponseAsync(HttpClient client, HttpResponseMessage response, CancellationToken cancellationToken) {
         var status = (int)response.StatusCode;
         if (status == 401) {
             await configuration.TokenService.Logout();
         }
    }
}