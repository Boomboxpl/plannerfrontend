﻿namespace PlannerFrontend.Clients; 

public class ClientConfigurationService : IClientConfigurationService {
    public ITokenService TokenService {get; private set;}
    
    public ClientConfigurationService(ITokenService tokenService) {
        TokenService = tokenService;
    }
}