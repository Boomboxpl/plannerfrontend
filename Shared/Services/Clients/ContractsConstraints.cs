﻿using Blazorise;
using PlannerFrontend.Utils;

namespace PlannerFrontend.Clients; 

public partial interface IPropositionsClientService {
    public int NameMinLength => 5;
    
    public void NameValidation(ValidatorEventArgs e) {
        ValidationUtils.MinLengthValidation(e, NameMinLength);
    }
}

public partial interface IAuthorizationClientService {
    public int PasswordMinLength => 2;
    public int UsernameMinLength => 3;
    public int UsernameMaxLength => 20;
    
    public void PasswordValidation(ValidatorEventArgs e) {
        ValidationUtils.MinLengthValidation(e, PasswordMinLength);
    }
    
    public void UsernameValidation(ValidatorEventArgs e) {
        e.OnTrue(ValidationRule.IsLength(e.Value as string, UsernameMinLength, UsernameMaxLength));
    }
}
    
public partial interface IEventsClientService {
    public int NameMinLength => 5;
    public double MinDuration => 15;
    
    public void NameValidation(ValidatorEventArgs e) {
        ValidationUtils.MinLengthValidation(e, NameMinLength);
    }

    public void DurationValidation(ValidatorEventArgs e) {
        e.OnTrue((double)e.Value >= MinDuration);
    }
}