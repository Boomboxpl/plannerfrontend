﻿namespace PlannerFrontend.Clients; 

public interface IClientConfigurationService {
    public ITokenService TokenService { get; }
}