﻿using Blazorise;
using PlannerFrontend.Clients;

namespace PlannerFrontend.Utils; 

public static class ValidationUtils {
    public static void OnTrue(this ValidatorEventArgs e, bool value) {
        e.Status = value ? ValidationStatus.Success : ValidationStatus.Error;
    }

    public static void ValidTimeRange(this ValidatorEventArgs e, TimeSpan start, TimeSpan end, TimeSpan? minDuration = null) {
        minDuration ??= new TimeSpan(0);
        e.OnTrue(start + minDuration <= end);
    }
    
    public static void NotNull(ValidatorEventArgs e) {
        NotNull(e, e.Value);
    }

    public static void NotNull(ValidatorEventArgs e, object? val) {
        e.OnTrue(val != null);
    }
    
    public static void NotEmpty<T>(ValidatorEventArgs e, ICollection<T>? collection) {
        var isNotEmpty = (collection?.Any() ?? false);
        e.OnTrue(isNotEmpty);
    }

    public static void IsEqual(ValidatorEventArgs e, string? secondValue) {
        string? value = e.Value as string;
        bool isEqual = !string.IsNullOrEmpty(value) && ValidationRule.IsEqual(value, secondValue);
        e.OnTrue(isEqual);
    }

    public static void MinLengthValidation(ValidatorEventArgs e, int minLength) {
        string? value = e.Value as string;
        e. OnTrue(!string.IsNullOrEmpty(value) && value.Length > minLength);
    }
}