﻿using Blazorise;

namespace PlannerFrontend.Utils; 

public static class LayoutUtils {
    public static IFluentSpacingOnBreakpointWithSideAndSize PagePadding => Padding.Is4.OnAll;
    public static HeadingSize MainHeaderSize => HeadingSize.Is3;
}