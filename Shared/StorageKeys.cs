﻿namespace PlannerFrontend.Shared; 

public class StorageKeys {
    public const string AccessToken = "accessToken";
    public const string RefreshToken = "refreshToken";
}