﻿using Blazored.SessionStorage.Serialization;
using Newtonsoft.Json;

namespace PlannerFrontend; 

public class NewtonSoftJsonSerializer : IJsonSerializer
{
    public T Deserialize<T>(string text) 
        =>JsonConvert.DeserializeObject<T>(text);

    public string Serialize<T>(T obj) 
        => JsonConvert.SerializeObject(obj);
}